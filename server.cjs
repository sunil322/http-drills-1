const http = require('http');
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');
const PORT = process.env.port || 8000;

const requestListener = ((request, response) => {

    const statusCode = request.url.split('/')[2];
    const delayInSeconds = request.url.split('/')[2];

    if (request.url === '/html' && request.method === 'GET') {
        fs.readFile('./index.html', 'utf-8', (error, data) => {
            if (error) {
                console.log(error);
                response.writeHead(500, { 'Content-Type': 'application/json' });
                const errorMessage = {
                    message: 'Facing problem in reading html file'
                }
                response.write(JSON.stringify(errorMessage));
                response.end();
            } else {
                response.writeHead(200, { 'Content-Type': 'text/html' });
                response.write(data);
                response.end();
            }
        });
    } else if (request.url === '/json' && request.method === 'GET') {
        if (fs.readFile('./data.json', 'utf-8', (error, data) => {
            if (error) {
                console.log(error);
                response.writeHead(500, { 'Content-Type': 'application/json' });
                const errorMessage = {
                    message: 'Facing problem in reading json file'
                }
                response.write(JSON.stringify(errorMessage));
                response.end();
            } else {
                response.writeHead(200, { 'Content-Type': 'application/json' });
                response.write(data);
                response.end();
            }
        }));
    } else if (request.url === '/uuid' && request.method === 'GET') {
        response.writeHead(200, { 'Content-Type': 'application/json' });
        const uuidObject = {
            uuid: uuidv4()
        };
        response.write(JSON.stringify(uuidObject));
        response.end();
    } else if (request.url === `/status/${statusCode}` && request.method === 'GET') {
        try {
            response.writeHead(statusCode, { 'Content-Type': 'application/json' });
            const statusMessage = {
                status: `${statusCode} ${http.STATUS_CODES[statusCode]}`
            };
            response.write(JSON.stringify(statusMessage));
            response.end();
        } catch {
            response.writeHead(400, { 'Content-Type': 'application/json' });
            const errorMessage = {
                message: "Invalid status code"
            };
            response.write(JSON.stringify(errorMessage));
            response.end();
        }

    } else if (request.url === `/delay/${delayInSeconds}` && request.method === 'GET') {
        const seconds = Number(delayInSeconds);
        if (isNaN(seconds)) {
            response.writeHead(400, { 'Content-Type': 'application/json' });
            const errorMessage = {
                message: "Seconds should be a number"
            }
            response.write(JSON.stringify(errorMessage));
            response.end();
        } else if (seconds < 0) {
            const errorMessage = {
                message: "Seconds should be equal or greater than 0"
            }
            response.writeHead(400, { 'Content-Type': 'application/json' });
            response.write(JSON.stringify(errorMessage));
            response.end();
        }
        else {
            setTimeout(() => {
                response.writeHead(200, { 'Content-Type': 'application/json' });
                const delayTimes = {
                    'delayInseconds': seconds
                };
                response.write(JSON.stringify(delayTimes));
                response.end();
            }, seconds * 1000);
        }
    } else {
        response.writeHead(404, { 'Content-Type': 'application/json' });
        const errorMessage = {
            message: "This url doesn't exist"
        }
        response.write(JSON.stringify(errorMessage));
        response.end();
    }
});

const server = http.createServer(requestListener);

server.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});